# Teste de Lógica 1 - NEPPO (Python 3.6.1)
# Wilson Valente
# 09/02/2018
#!/usr/bin/python
# -*- coding: utf-8 -*-

from numpy import uint64

while True:
  sequenciaNumerica = input("Entre com uma sequência numérica: ")
  saida = ''
  try:
    hasLongInteger = int(sequenciaNumerica)
    #pass
  
    contador = 0
    
    # Varre a entrada de inteiros como string
    for x in range(0, len(sequenciaNumerica)):
    
      # EasterEgg
      if (sequenciaNumerica == '42'):
        print ('Essa é a resposta especial para o significado da vida, do universo e tudo mais.')
        break
      
      # Verifica se existe próxima posição
      if (x+1 < len(sequenciaNumerica)):
        
        # Se existe entao compara a próxima posição com a atual
        # Se for diferente, imprime o contador, imprime o valor da posição e Zera o contador
        if (sequenciaNumerica[x+1] != sequenciaNumerica[x]):
          saida = saida + str(contador+1) + (sequenciaNumerica[x])
          #print (contador+1, end="", flush=True)
          #print (sequenciaNumerica[x], end="", flush=True)
          contador = 0
        
        # Se não for diferente, soma 1 ao contador
        else:
          contador = contador + 1
      
      # Não existe próxima posição, imprime a última contagem (contador) e o último valor da sequenciaNumerica.
      else:
        #print ('Nao existe proxima posicao')
        saida = saida + str(contador+1) + str(sequenciaNumerica[x])
        #print (contador+1, end="", flush=True)
        #print (sequenciaNumerica[x])
    print()
    print(saida) # Não gera erro de Long Int com números grandes
    print(uint64(saida))
    print()
    #break
    
  except ValueError:
    print('Entre com um inteiro.')
    print()
    

''' Obsevações 
Python trata uma entrada (input) qualquer como String. Por isso a facilidade de verificar as sequências de números da entrada dada.

Porém como era necessário não aceitar qualquer valor que não fosse um inteiro, foi incluído um try/catch que verifica se o valor é um inteiro.

Foi observado que o teste pedia uma entrada de inteiro longo sem sinal, porém o problema de lógica foi resolvido de uma forma mais simples somente testando essa entrada (para saber se é inteiro) e usando tratamento de String da linguagem para simplificar a saída.

A saída, forçadamente foi colocada como inteiro longo sem sinal de 64 bits. O teste de entrada para esse tipo (uint64), para números grandes resulta em erro de parse long int unsigned para compilção final. Como se trata de um teste, foi poupado tempo nessas questões descritas.
'''

