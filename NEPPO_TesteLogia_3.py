# Teste de Lógica 3 - NEPPO (Python 3.6.1)
# Wilson Valente
# 10/02/2018
#!/usr/bin/python
# -*- coding: utf-8 -*-

def escreveRomano (n):
  
  if (n >= 1000):
    print ('M', end='', flush=True)
    return escreveRomano(n-1000)
    
  elif (n >= 900):
      print ('CM', end='', flush=True)
      return escreveRomano(n-900)
      
  elif (n >= 500):
    print ('D', end='', flush=True)
    return escreveRomano(n-500)
  
  elif (n >= 400):
      print ('CD', end='', flush=True)
      return escreveRomano(n-400)
      
  elif (n >= 100):
    print ('C', end='', flush=True)
    return escreveRomano(n-100)
  
  elif (n >= 90):
    print ('XC', end='', flush=True)
    return escreveRomano(n-90)
    
  elif (n >= 50):
    print ('L', end='', flush=True)
    return escreveRomano(n-50)
  
  elif (n >= 40):
    print ('XL', end='', flush=True)
    return escreveRomano(n-40)
  
  elif (n >= 10):
    print ('X', end='', flush=True)
    return escreveRomano(n-10)
  
  elif (n >= 9):
    print ('IX', end='', flush=True)
    return escreveRomano(n-9)
  
  elif (n >= 5):
    print ('V', end='', flush=True)
    return escreveRomano(n-5)
  
  elif (n >= 4):
    print ('IV', end='', flush=True)
    return escreveRomano(n-4)
    
  elif (n >= 1):
    print ('I', end='', flush=True)
    return escreveRomano(n-1)
  
  # Caso base 
  elif (n <= 0):
    print ()
    

# Entrada
numeroDecimal = int(input("Entre com um número: "))

# Chamada para funcao
escreveRomano(numeroDecimal)

