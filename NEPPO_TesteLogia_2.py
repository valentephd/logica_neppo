# Teste de Lógica 2 - NEPPO (Python 3.6.1)
# Wilson Valente
# 09/02/2018
#!/usr/bin/python
# -*- coding: utf-8 -*-

data = input('Entre com a data: ')

dia, mes, ano = data.split()

dia = int(dia)
mes = int(mes)
ano = int(ano)

# Meses que vão até 30 (menor que 31) independete do ano ser bissexto ou não
if (mes in (4, 6, 9, 11)) and (dia in range(1,31)):
  #print ('ok') #saída tem que ser boolean
  saida = True
  print (saida)
  
# Meses que vão até 31 independete do ano ser bissexto ou não
elif (mes in (1, 3, 5, 7, 8, 10, 12)) and (dia in range(1,32)):
  #print ('ok') #saída tem que ser boolean
  saida = True
  print (saida)
  
# Fevereiro ! Hora de acertar a volta ao mundo
elif (mes == 2):
  
  # Fevereiro em ano bissexto (aceita inclusive o dia 29)
  if ((ano%4 == 0) and ( (ano%100 != 0) or (ano%400 == 0) )) and (dia in range(1,30)):
    #print ('É bissexto.')
    #print ('ok') #saída tem que ser boolean
    saida = True
    print (saida)
  
  # Fevereiro em ano não bissexto (só aceita dias menores que 29 - 28, 27 ... 1)
  elif (dia in range(1,29)):
    #print ('ok') #saída tem que ser boolean
    saida = True
    print (saida)
    
  else:
    #print ('erro date !!!!') #saída tem que ser boolean
    saida = False
    print (saida)
  
else:
  #print ('erro date !!!!') #saída tem que ser boolean
  saida = False
  print (saida)


''' Observações

<strike>Não foram tratadas datas negativas !</strike> (Resolvido)

'''